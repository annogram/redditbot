# SCP-1500
## The SCP article fetcher

This robot will operate on [reddit](http://www.reddit.com). 

Its primary directive is to fetch _scp artciles_ from the main [SCP articles](http://www.scp-wiki.net), these articles are scraped from the html response and converted to markdown.

## How to call SCP-1500
This bot will be called whenever a user enters a string in the format `!scp-xxx` or `!scp-xxxx` in any comment on reddit. Users will also be able to add thier favourite articles
using the command `!fav scp-xxxx` and view their favourited articles using `!myfav`.

## Dependencies

This project was origionally made in python but then ported to Nodejs. To install this application download the repository and then from
the root directory run `npm install` to install all operating dependencies.


## Setup
This system is designed to run on a raspberry pi

You need to create an auth token, either manually or using [this](https://github.com/not-an-aardvark/reddit-oauth-helper) program.

You will need to set up a firebase database and pass in the apo credentials as a dbconfing object

Below is structure of the environment variables that need to be passed to the project.

> note that storing json object in environment variables requries you to use _expicit_ json ie. all keys are strings surronded by quotation marks, see below.

```JSON
{
    "USERNAME": "",
    "PASSWORD": "",
    "CLIENT_ID": "",
    "CLIENT_SECRET": "",
    "TOKEN": { "access_token": "",
        "token_type": "bearer",
        "expires_in": 3600,
        "refresh_token": "",
        "scope": "read submit" },
    "DBConfig" : {
      "apiKey" : "",
      "authDomain": "",
      "databaseURL" : ""
    }
}
```

The username and password fields are associated to the bots reddit account. the `CLIENT_ID` and `CLIENT_SECRET` are the keys given to you by reddit
when you start a new script project from the developers section. The `TOKEN` is required by snoowrap to poll reddit to the api rules, follow the 
npm guilines for acquiring this token on the snoowrap npm page. `DBConfig` is the firebase database access required to store the user favourites.

## How to run

This script is meant to be an _always on_ script, it is recommended to use the npm program `forever` to run this program. To run on a raspberry pi
ensure there is an appropriate version of node running and call the command `nohup forever app.js &` to run the application as the pi user and keep 
it running when you disconnect from ssh.

## Docker

docker run -e ENVAR=var ... 