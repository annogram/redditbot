const fs = require('fs');
const database = require('./DAL/DBContext');
const base = 'http://www.scp-wiki.net/';
const cheerio = require('cheerio');
const request = require('request');
const desc = '\n\n---\nI am a~~n anomalous entity~~ bot. Bleep. Bloop. I report articles from '
+ 'the creative writing website '
+ '[The SCP Foundation](http://www.scp-wiki.net/about-the-scp-foundation).\n\n'
+ 'To retrieve artciles |Add an Article to your favourites | Your favorites | Message the creator\n'
+ ':----: | :----: | :----: | :----:\n'
+ '`!scp-XXXX` |  `!fav scp-XXXX` | `!myfav` | [Click here to message me](http://np.reddit.com/message/compose/?to=ZacharyCallahan&subject=SCP1500&message=Hey Zach)';

/**
 * Handle all users favourites
 */
class Favourite {
  constructor() { }

  /**
   * Save users favourite scp.
   * 
   * @param {string} user 
   * @param {string} scp 
   */
  add(user, scp) {
    console.info('Adding favourite to user list');
    return new Promise((resolve, reject) => {
      getTitle(scp).then(title => {
        let scpValue = `[${title}](${base + scp})`;
        Promise.resolve(database.set({
          key: 'fav/' + user + '/' + scp,
          value: scpValue
        })).then(() => {
          resolve();
        });
      }).catch(err => {
        console.error(err);
      });
    })
  }

  /**
   * Get a users favourite scp
   * 
   * @param {string} user 
   */
  get(user) {
    console.info('Getting a users favourites');
    return new Promise((resolve, reject) => {
      let response = '';
      database.getSnapshot(`fav/${user}`).then(favourites => {
        if (favourites === null) {
          response = 'You got no favourites mate.';
        } else {
          response = `## ${user}'s favourites:\n\n`;
          for (let key in favourites) {
            response += `* ${favourites[key]}\n`;
          }
        }
        response = response.substr(0, 10000 - desc.length) + desc;
        resolve(response);
      });
    });
  }
}

module.exports = new Favourite();

function getTitle(scpName) {
  let series = /(?=(scp-(?:[\w:_$\?\-\!])*-j))|(?=scp-(\d{3,4}))/gi.exec(scpName);
  let title = 'joke';
  let requestUrl = 'http://www.scp-wiki.net/scp-series';
  return new Promise((resolve, reject) => {
    if (series[1] === undefined) {
      if (series[2] >= 1000 && series[2] < 2000) {
        requestUrl += '-2';
      } else if (series[2] >= 2000 && series[2] < 3000) {
        requestUrl += '-3';
      } else if (series[2] >= 3000 && series[2] < 4000) {
        requestUrl += '-4';
      }
    }
    request(requestUrl, (err, res, data) => {
      if (err) reject(err);
      let $ = cheerio.load(data);
      let scpNo = new RegExp('SCP-' + series[2], 'i');
      $('ul li').each((i, e) => {
        if (scpNo.test($(e).find('a').text())) {
          resolve($(e).text());
        }
      });
    });
  });
}