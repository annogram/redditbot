'use strict'
const cheerio = require('cheerio');
const request = require('request');
const toMarkdown = require('to-markdown');
const base = 'http://www.scp-wiki.net/';
const desc = '\n\n---\nI am a~~n anomalous entity~~ bot. Bleep. Bloop. I report articles from '
  + 'the creative writing website '
  + '[The SCP Foundation](http://www.scp-wiki.net/about-the-scp-foundation).\n\n'
  + 'To retrieve artciles |Add an Article to your favourites | Your favorites | Message the creator\n'
  + ':----: | :----: | :----: | :----:\n'
  + '`!scp-XXXX` |  `!fav scp-XXXX` | `!myfav` | [Click here to message me](http://np.reddit.com/message/compose/?to=ZacharyCallahan&subject=SCP1500&message=Hey Zach)';
/**
 * Scrape the SCP wikidot site for information and convert it to
 * reddit friendly text.
 */
class SCPSiteScrapper {
  constructor() { 
    this.link = '';
  }

  /**
   * Scrape the website for an article named in the form scp-xxx/xxxx
   * 
   * @param {string} number 
   */
  scrape(number) {
    this.link = base + number;
    console.info('url:',this.link);

    return new Promise((resolve, reject) => {
      request(this.link, (err, res, data) => {
        let article = this.extract(data);
        resolve(article);
      });
    });
  }

  extract(webpage) {
    // first pass
    let $ = cheerio.load(webpage);
    let rating = $('div.heritage-rating-module span.number').text();
    if (rating === undefined) {
      rating = $('.rate-points .number').text();
    }
    let returnable = '';
    // Remove voting module
    $('div.heritage-rating-module').remove();
    $('div.page-rate-widget-box').remove();
  
    // Second pass
    let firstPass = $('#page-content').html();
    $ = cheerio.load(firstPass);
  
    // make all links non-relative
    $('a').each((i, e) => {
      let base2 = base.substring(0, base.length - 1);
      $(e).attr('href', base2 + $(e).attr('href'));
    });
  
    // Convert all image tags into link tags for RES users.
    $('div.scp-image-block').each((i, e) => {
      // Grab caption of images if there is one
      let caption = 'Image';
      let captionBlock = $(e).find('div.scp-image-caption')
      if (captionBlock.text() !== undefined)
        caption = captionBlock.text();
      // if no caption text should read 'Image'
      let img = $(e).find('img');
      $(e).replaceWith(`<a href="${img.attr('src')}">${caption}</a>`);
    });
  
    // Superscript
    $('sup').replaceWith('^' + $('sup').text());
  
    // Subscript
    $('sub').replaceWith($('sub').text());
  
    // Strikethrough
    $('span').each((i, e) => {
      if (/line-through/i.test(($(e).attr('style')))) {
        $(e).replaceWith(`~~${$(e).text()}~~`);
      }
    });
  
    // Convert html 5 tables
    let tables = [];
    $('table').each((i, e) => {
      tables.push(makeTable($(e)));
      $(e).replaceWith(';;;' + i + ';;;');
    });
  
    
    // Remove Span tags
    $('span').replaceWith($('span').text());
    
    // Remove tt tags
    $('tt').replaceWith($('tt').text());
    
    // Delete IFrames
    $('iframe').replaceWith(`[_Format screw_ visit original article](${this.link})`);

    // Open collapasble blocks
    let collapsed = [];
    $('.collapsible-block').each((index, elememt) => {
      let closed, open, content, replacement;
      closed = $(elememt).find('.collapsible-block-folded .collapsible-block-link').text();
      open = $(elememt).find('.collapsible-block-unfolded-link .collapsible-block-link').text();
      content = toMarkdown($(elememt).find('.collapsible-block-content').html());
      replacement = `-${closed}\n\n+${open}\n\n${content}`;
      collapsed.push(replacement);
      $(elememt).replaceWith(`;;${index};;`);
    });
    
    // Add link to article
    $('p').each((i, e) => {
      let content = $(e).text();
      let re = /Item #: SCP-\d{3,4}/i;
      let itemNo = re.exec(content);
      if(itemNo){
        let newContent = content.replace(re, `<br />[${itemNo[0]}](${this.link})`);
        $(e).replaceWith(newContent);
      }
    });

    // Remove all unprocessed htmltags do this last
    $('div').remove();
    returnable += toMarkdown($.html());
  
    collapsed.forEach(function(element, i) {
      let replace = ';;' + i + ';;';
      returnable = returnable.replace(replace, element);
    }, this);

    tables.forEach((e, i) => {
      let replace = ';;;' + i + ';;;';
      returnable = returnable.replace(replace, e);
    }, this);
  
    // trim 
    returnable = returnable.substr(0, 10000 - desc.length) + desc;
  
    return returnable;
  }
}

module.exports = SCPSiteScrapper;

/**
 * Make a Markdown table from an HTML5Table
 * 
 * @param {Cheerio} html5Table 
 */
function makeTable(html5Table) {
  let rowString = '';
  let $ = cheerio.load(html5Table.html());
  html5Table.each((ti, te) => {
    let rows = $(te).find('tr'),
      colIndex = 0;

    // make headers
    $(te).find('th').each((hi, he) => {
      rowString += $(he).text() + ' | ';
      colIndex++;
    });
    rowString += '\n';

    // If there are no headers fuck it.
    if (colIndex === 0) return;

    for (let i = 0; i < colIndex; i++) rowString += ' :---- |';

    // Populate table
    rows.each((ri, re) => {
      $(re).find('td').each((ci, ce) => {
        rowString += $(ce).text() + ' | ';
      });
      rowString += '\n';
    });
    // $(te).replaceWith(rowString);
  });
  return rowString;
}