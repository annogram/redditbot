'use strict'
const firebase = require('firebase');
const DBConfig = JSON.parse(process.env.DBConfig);
/**
 * Make agnostic DB wrapper for firebase.
 */
class DBContext {
  constructor() {
    firebase.initializeApp(DBConfig);
    firebase.auth().signInAnonymously();
    this.db = firebase.database();
  }

  /**
   * Set the value of something in the firebase db.
   * 
   * @param {*} obj 
   */
  set(obj) {
    this.db.ref(obj.key).set(obj.value);
  }

  /**
   * Retrieve a snapshot from the database.
   * @param {string} key 
   * @param {function} callback 
   */
  getSnapshot(key) {
    return new Promise((resolve, reject) => {
      this.db.ref(key).once('value')
      .then(snapshot => {
        resolve(snapshot.val());
      });
    });
  }
}

let single = new DBContext();
module.exports = single;