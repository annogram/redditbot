const Reddit = require('reddit-snooper');
const Snoowrap = require('snoowrap');
const Snoostorm = require('snoostorm');

const TOKEN = JSON.parse(process.env.TOKEN);
const snoo = new Snoowrap({
  userAgent: process.env.USERNAME,
  accessToken: TOKEN.access_token,
  clientId: process.env.CLIENT_ID,
  clientSecret: process.env.CLIENT_SECRET,
  refreshToken: TOKEN.refresh_token
});

// Create reddit object
const r = new Reddit({
  username: process.env.USERNAME,
  password: process.env.PASSWORD,
  app_id: process.env.CLIENT_ID, // eslint-disable-line camelcase
  api_secret: process.env.CLIENT_SECRET, // eslint-disable-line camelcase
  automatic_retries: true, // eslint-disable-line camelcase
  api_requests_per_minuite: 60 // eslint-disable-line camelcase
});

class RedditInterface {
  constructor(sr) {
    this.r = snoo;
    this.commentStream = new Snoostorm(snoo).CommentStream({
      subreddit: sr,
      results: 100
    });
    this.flagfinder = {
      article: /(?=!(scp-(?:[\w:_$\?\-\!])*-j))|(?=!(scp-\d{3,4}))/gi,
      favourite: /(?=!fav\s(scp-(?:[\w:_$\?\-\!])*-j))|(?=!fav\s(scp-\d{3,4}))/gi,
      get: /(?=(!myfav))/gi
    }
  }

  watch(callback) {
    console.info('Watching');
    this.commentStream.on('comment', comment => {
      let c = comment.body;
      if(comment.author.name != process.env.USERNAME){
        for (let key in this.flagfinder) {
          if (this.flagfinder[key].test(c)) {
            let expr = this.flagfinder[key].exec(c);
            let data = (expr[1] === undefined) ? expr[2] : expr[1];
            callback.call(this, key, comment, data);
          }
        }
      }else{
        console.info('My comment');
      }
    });
  }

  respond(comment, article) {
    comment.reply(article);
  }
}
/**
 * Delete a comment using the reddit api endpoint.
 * @param {string} commentName 
 */
function deleteComment(commentName) {
  r.api.post('/api/del',
    {
      api_type: 'json',
      id: commentName
    },
    (err, resCode, data) => {
      // callback logic
      if (err) throw err;
      console.info('Deleted.');
    });
}

module.exports = RedditInterface;
