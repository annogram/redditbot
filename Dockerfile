FROM node
EXPOSE 8888

WORKDIR /usr/local/
ADD . /usr/local/
RUN npm install
ENTRYPOINT [ "node", "./app.js" ]
CMD [ "-c" ]