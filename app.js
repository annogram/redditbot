const Bot = require('./src/RedditInterface');
const SCPSiteScrapper = require('./src/Scrapper');
const favourite = require('./src/Favourite');

// Start Reddit interface
let bot = new Bot('all');
// Start the scrapper
let scrapper = new SCPSiteScrapper(__dirname);
// Start the favourites keeper
// Start watching the comment stream

bot.watch(watcher);


function watcher(type, comment, data) {
  console.info('Recieved command:', type);
  switch (type) {
    case 'favourite':
      // If the favourite command was called add to that users favourites
      favourite.add(comment.author.name, data)
        .then(() => {
          favourite.get(comment.author.name).then(faves => {
            bot.respond(comment, `Added ${data} to your favourites.\n\n` + faves);
          });
        });
      break;
    case 'get':
      // Get the favourites of a user.
      favourite.get(comment.author.name).then(faves => {
        bot.respond(comment, faves);
      });
      break;
    case 'article':
      // If the article command was called scrape an article, parse it, and send it back
      scrapper.scrape(data)
        .then(response => {
          bot.respond(comment, response);
        }).catch(err => {
          console.error(err);
        });
    default:
      break;
  }
}